---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=2cm
fontfamily:
- charter
---
\begin{titlepage}
\begin{center}
    \vspace*{2cm}

    \textbf{Analyse de programmes pour la sécurité logicielle}

        INF889A

    \vspace{3cm}

    \textbf{Alexandre Côté Cyr - COTA07049302}

    \vspace{3cm}

        Présentation d'un outil\\
        \textbf{SLEIGH - Un langage pour la spécification rapide de processeurs}

    \vspace{3cm}

        Travail remis\\
        à\\
        Jean Privat

    \vfill

    Université du Québec à Montréal\\
    28 Janvier 2020

    \vspace*{2cm}
\end{center}
\end{titlepage}

\thispagestyle{empty}
\clearpage
\pagenumbering{arabic}
\setcounter{page}{1}

# SLEIGH - Un langage pour la spécification rapide de processeurs

SLEIGH est un outil pour décrire les instructions machines, l'organisation de la mémoire des registres ainsi que l'ABI utilisée par des processeurs.

SLEIGH est un terme englobant pour le langage de définition qu'il comporte, le compilateur pour ce langage, le P-Code -- représentation intermédiaire vers laquelle il compile -- et la bibliothèque logicielle qui contient tous ces éléments. Cet ensemble d'outil forme le coeur de l'engin de décompilation du logiciel Ghidra.[^1]

SLEIGH est aussi disponible comme une bibliothèque qui peut être utilisée séparément de Ghidra.

[^1]: NSA. _SLEIGH - A Language for Rapid Processor Specification_.

## Historique

SLEIGH est développé par la NSA depuis au moins 2005 et a été rendu _Open-Source_ en 2019 en même temps que Ghidra.

Le langage est basé sur SLED[^2], un langage de définition d'instructions machine développé en 1997 par un chercheur de l'Université de Virginie et une chercheuse de _AT&T Labs_.

Aucune publication scientifique sur SLEIGH ni sur le décompilateur de Ghidra n'a été diffusée publiquement, nous en savons donc peu sur le raisonnement ayant mené à cette conception ainsi que sur les mécanismes utilisés.

[^2]: Fernàndez, Mary F. et Ramsey, Norman. _Specifying Representations of Machine Instructions_

## Composantes

### Langage de définition de processeur

Une spécification SLEIGH se divise en trois parties principales. Ces parties peuvent être entremêlées dans le fichier, mais un élément doit absolument être défini avant son utilisation.

La première section est la définition des caractéristiques du processeur. L'instruction `define` est utilisée ici.

Dans cette section, on définit le boutisme du processeur, l'alignement des instructions en mémoire, les registres et leurs caractéristiques, les indicateurs d'états (_Flags_)  ainsi que les éventuelles autres zones de mémoire disponibles.
La définition des registres et zones mémoire doit contenir leurs noms, leurs tailles et la taille d'un Mot dans cet espace.

Le langage SLEIGH permet la manipulation de bits individuels, même si le plus petit élément manipulé par le P-Code est un octet. Les instructions de manipulations de bits sont donc transformées en une suite d'opérations bit à bit, de décalages, de tronquages et d'extensions lorsque compilées


La seconde section d'une spécification est la définition des symboles. Ces symboles sont des variables qui seront liées à des registres, adresses ou valeurs immédiates au moment du désassemblage d'un binaire.
Les symboles incluent aussi les _Tokens_, ceux-ci indiquent comment découper une instruction en ses parties constitutrices (code d'opération, opérandes, registres, etc.) et sont utilisés dans la définition des instructions.

Des variables de contexte peuvent aussi être déclarées dans cette section. Celles-ci servent à représenter l'état du processeur lorsque celui-ci peut avoir un impact sur l'interprétation des instructions. Par exemples, dans une architecture ARM, on utiliserait une variable de contexte pour déterminer si le processeur est en mode normal ou en mode Thumb.


La troisième section est la définition de Constructeurs. Dans cette section, on définit les instructions du processeur. Ceci inclue la représentation des instructions, les opérandes qu'elles utilisent et leur sémantique. Les effets des instructions doivent tous être définies explicitement, ceci inclue la modification des indicateurs d'états, des pointeurs de pile, etc.

Les informations contenues dans les Constructeurs seront utilisées par le désassembleur pour identifier et découper les instructions ainsi que pour afficher leur représentation mnémonique. La partie sémantique des instructions sera compilée en P-Code, ce qui permettra au décompilateur d'utiliser ses heuristiques pour récupérer une approximation de code source des programmes qu'il traitera.

### P-Code

Le P-Code est un _Register Transfer Language_ (un type de représentation intermédiaire pour les instructions machines). Cette représentation intermédiaire est générée par le compilateur SLEIGH à partir des définitions mentionnées dans la section précédente.

Il permet de définir quelles sont les instructions implémentées par le processeur ainsi que comment les représenter. Plus important encore, le P-Code permet de définir la sémantique et les effets des instructions sur l'état de la machine, soit sur la mémoire, les registres, les indicateurs d'état, etc.

Le décompilateur travaille à partir de cette représentation intermédiaire, il n'est donc pas nécessaire de modifier quoi que ce soit dans le code de décompilation lui-même lorsque l'on veut ajouter le support pour un nouveau type de processeur.


## Principes de la décompilation

Le décompilateur agit à partir de la représentation intermédiaire et utilise plusieurs techniques qui proviennent de la théorie des compilateurs tel que: _parsing_ du langage, génération d'AST pour le langage intermédiaire, manipulation et optimisation de l'arbre de syntaxe, un langage de tranfert de registre (P-Code), la forme _Static Single Assignment (SSA)_, les _Control Flow Graphs_,  des règles de réécriture, l'élimination de code mort ainsi que des tableaux de symboles et de portées.

En plus de ceux-ci, des concepts spécifiques à la décompilation sont utilisés. La reconnaissance de ces informations se fait à partir d'heuristiques qui, tel que mentionné dans la section [Historique](#historique), ne sont pas documentées en dehors du code source où elles sont définies.

__Fusion de variables__: À partir de l'utilisation des registres et emplacements en mémoire, le décompilateur tente de regrouper celles qui représentent la même variable dans le code source en C.

__Propagation des types__: En se basant sur les types des valeurs retournées, des paramètres et des opérandes ainsi qu'à partir du contenu et de l'utilisation de variables, le décompilateur tente d'assigner les types aux variables.

__Structuration du flot de contrôle__: Le décompilateur tente de recomposer la structure du programme (boucles, conditions, etc.) à partir des sauts, _goto_ et vérifications de contraites.

__Récupération de prototypes de fonction__: À partir des conventions d'appel définies, de l'utilisation des fonctions et des fichiers d'en-tête auxquels il a accès, le décompilateur essaie de retrouver les signatures de fonctions utilisées.

__Récupération d'expressions__: Le décompilateur interprète des suites d'instructions machine comme des expressions en langage C.


## Utilisation

La définition d'un processeur nécessite plusieurs fichiers.

Un fichier `.ldefs` contient les informations permettant d'identifier une architecture et ses métadonnées. Il contient aussi les références vers les autres fichiers de la définition.

Un fichier `.cspec` contient la définition d'un compilateur utilisé dans l'architecture. Ceci inclue l'alignement et la taille des types de données, l'utilisation des zones mémoires et la définition des conventions d'appel.

Le fichier `.pspec` sert à définir des informations spécifiques au processeur. On y définit donc les registres d'état, le pointeur d'instruction et les symboles particuliers utilisés par le processeur.

Finalement, un fichier `.slaspec` et, optionnellement, un ou plusieurs fichiers `.sinc` sont utilisés pour définir tout ce qui a trait au fonctionnement du processeur. Dans ces fichiers, on spécifie les registres, les zones mémoires utilisées, ainsi que le découpage des instructions, leur représentation et leur comportement. Les 3 sections expliquées dans la partie [Langage de définition de processeur](#langage-de-définition-de-processeur) se trouvent dans ces fichiers.

## Limites de l'outil

La compilation est un processus _lossy_. Les langages d'assemblages sont moins expressifs que les langages à haut niveau et des informations qui aident la compréhension, mais qui ne sont pas nécessaire à l'exécution, comme les noms de variables, ne cont pas conservées. La décompilation est donc nécessairement imparfaite.

Le décompîlateur a des problèmes à identifier des classes et structures de données automatiquement. Ceux-ci sont souvent identifiés comme une suite de variables de type de base. Il en est de même pour la reconnaissance de types. Le décompilateur confond souvent les types ayant la même taille.

La documentation du langage SLEIGH est incomplète. Pour certaines sections, comme la définition des processeurs, seule la syntaxe est définie complètement. La sémantique de cette partie n'est pas documentée.

SLEIGH n'est pas adapté à la définition d'appels systèmes, ceux-ci doivent être implémentés comme un hybride de fonctions au niveau utilisateur et d'instruction machines.

Les architectures moins communes ne sont pas supportées par l'outil.

L'analyse et la décompilation sont lentes pour des binaires de taille importantes.

\pagebreak
# Références

Fernàndez, Mary F. et Ramsey, Norman. _Specifying Representations of Machine Instructions_ (1997) \url{https://www.cs.tufts.edu/~nr/pubs/specifying.html}


NSA. _Decompiler Analysis Engine - SLEIGH_. \url{https://ghidra-decompiler-docs.netlify.com/sleigh.html}


NSA. _Ghidra Software Reverse Engineering Framework_. Github. \url{https://github.com/NationalSecurityAgency/ghidra}


NSA. _SLEIGH - A Language for Rapid Processor Specification_. (1er septembre 2017). \url{https://ghidra.re/courses/languages/html/sleigh.html}


Wikipedia. _Decompiler_. (15 janvier 2020). https://en.wikipedia.org/wiki/Decompiler

## Ouvrages et papiers connexes

Collberg, C. et Thomborson, Clark & Low, D.. _Breaking abstractions and unstructuring data structures._ (1998). 28-38. 10.1109/ICCL.1998.674154. \url{https://www.researchgate.net/publication/3746603_Breaking_abstractions_and_unstructuring_data_structures/link/00b7d51e34de4e64d5000000/download}


Proebsting, Todd A. et Watterson, Scott A. _Krakatoa: Decompilation in Java (Does Bytecode Reveal Source?)_. The University of Arizona. (Juin 2017). \url{https://static.usenix.org/publications/library/proceedings/coots97/full_papers/proebsting2/proebsting2.pdf}
