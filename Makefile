slides:=dist/COTA07049302-Slides_Outil-SLEIGH.pdf
document:=dist/COTA07049302-Document_Outil-SLEIGH.pdf

all: $(slides) $(document)

$(slides): $(notdir $(slides:.pdf=.md))
	pandoc $< -t beamer -o $@

$(document): $(notdir $(document:.pdf=.md))
	pandoc $< -o $@

clean:
	rm -rf dist/*
