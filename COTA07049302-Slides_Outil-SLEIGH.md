\title{SLEIGH - A Language for Rapid Processor Specification}
\author{Alexandre Côté Cyr}
\institute{UQAM - INF889A}
\maketitle

# Quoi?

SLEIGH est l'ensemble d'outils qui forme le coeur du décompilateur de Ghidra.

# Décompilation?

Prend un programme compilé et essaie de redonner du code source

Compilation est un processus _lossy_, donc la décompilation est imparfaite

# Ghidra?

Cadriciel de rétro-ingénierie développé par la NSA.

License Apache 2.0

Écrit en Java

Inclue désassembleur, décompilateur, graphes de flots de fonctions, plugins en Java ou en Python, etc.

Supporte plusieurs formats et architectures, incluant certaines en bytecode comme Java

[Ghidra sur Github](https://github.com/NationalSecurityAgency/ghidra)

# Décompilateur de Ghidra

Analyse automatique de data-flow à partir d'un binaire

## Concepts similaire à un compilateur

 - Génération et optimisation de l'AST de la représentation intermédiaire
 - Faire le lien entre les constructions en langage intermédiaire et en langage de sortie (C)
 - _Static Single Assignment (SSA) form_
 - _Control Flow Graphs_
 - Règles de réécriture et élimination de code mort
 - Tableaux de symboles et de portées

## Concepts spécifiques à un décompilateur

 - Fusion de variables
 - Propagation des types
 - Structuration de flot de contrôle
 - Récupération de prototypes de fonctions
 - Récupérations d'expressions

[https://ghidra-decompiler-docs.netlify.com/](https://ghidra-decompiler-docs.netlify.com/)

# SLEIGH

## Historique

 - Basé sur SLED - a Specification Language for Encoding and Decoding.
 - Développé par la NSA
 - Développé depuis au moins 2005
 - Rendu Open-Source en 2019 en même temps que Ghidra

----

## Utilisation

Permet de définir les caractéristiques d'une architecture de processeur.

 - Registres et leurs tailles
 - Instructions, leur alignement, leur représentation et leur effet

Permet de définir l'ABI utilisée avec un OS/Compilateur

 - Structures de données et leur taille
 - Conventions d'appel

-- Définir syscalls comme fonctions User

----

## Syntaxe

Pas complètement documenté.

[https://ghidra.re/courses/languages/html/sleigh.html](https://ghidra.re/courses/languages/html/sleigh.html)

----

## Démo du langage

[https://gitlab.com/CycleOfTheAbsurd/ghidra_processor_nios_II/tree/master/data/languages](https://gitlab.com/CycleOfTheAbsurd/ghidra_processor_nios_II/tree/master/data/languages)

----

## Représentation Intermédiare

La définition du processeur en SLEIGH est compilée vers un format xml dans un fichier .sla

### P-Code

Langage de transfert de registres

 - Indépendant de la machine (représentation intermédiaire
 - Modélise des processeurs généraux
 - Agit sur des registres et emplacements mémoires
 - Aucun effet de bord, toutes les manipulations sont explicites
 - Les instructions ressembles à des instructions typiques de processeurs

 - Représentaion intermédiaire permet d'émuler l'exécution du code avec le [SLEIGH Emulator](https://ghidra-decompiler-docs.netlify.com/sleighapiemulate)

----

## Démo du décompilateur

![](./ghidra_decompiler_picture.png)

----

## Problèmes

 - Décompilation d'un binaire est nécessairement imparfaite
 - Mal documenté
 - Architectures moins communes pas toutes supportées
 - Mauvaise reconnaissances des classes et structures de données
 - Problème de reconnaissances d'array et de types
 - SLEIGH pas tout à fait adapté à définition de syscalls.
 - Problèmes de performance avec gros binaires
